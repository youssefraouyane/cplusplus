#include "marvel.hpp"

Personne::Personne()
{
	prenom="Inconnu";
	nom ="Inconnu";
	genrePersonne = Personne::INDETERMINE;

}

Personne::Personne(const char * p_prenom, const char * p_nom,  Personne::Genre p_genrePersonne):prenom(p_prenom),nom(p_nom)
{
	genrePersonne = p_genrePersonne;

}

Personne::Personne(const char * p_prenom, const char * p_nom):prenom(p_prenom),nom(p_nom)
{
	genrePersonne = Personne::INDETERMINE;
}


Personne::~Personne()
{

}

Personne Personne::INCONNU;

std::string Personne::getPrenom()const
{
	return prenom.c_str();
}

std::string Personne::getNom()const
{
	return nom.c_str();
}

Personne::Genre Personne::getGenre()const
{
	return genrePersonne;
}

void Personne::afficher(std::stringstream & ss)const
{
	std::string genre = "";
	if(genrePersonne == HOMME)
	{
		genre = "HOMME";
	}
	else if(genrePersonne == FEMME)
	{
		genre = "FEMME";
	}
	else
	{
		genre = "INDETERMINE";
	}
	ss<<this->getPrenom()<<" "<<this->getNom()<<" "<<"["<<genre<<"]";
}

void Personne::setNom(std::string p_nom)
{
	nom=p_nom;
}

void Personne::setPrenom(std::string p_prenom)
{
	prenom = p_prenom;
}

void Personne::setGenre(Personne::Genre p_genrePersonne)
{
	genrePersonne = p_genrePersonne;
}

std::stringstream& operator<<(std::stringstream & ss, const Personne & p)
{
	p.afficher(ss);
	return ss;
}

bool operator==(const Personne & a, const Personne & b)
{
	bool comparison = false;
	if(a.getNom()==b.getNom() && a.getPrenom()==b.getPrenom() && a.getGenre()==b.getGenre())
	{
		comparison = true;
	}
	return comparison;
}


bool operator!=(const Personne & a, const Personne & b)
{
	return !(a==b);
}


/**************************SUPER*************************************/


Super::Super()
{
	anonyme =true;
	niveau =0;
}

Super::~Super()
{

}

Super::Super(std::string p_nomDeScene, Personne p)
{
	anonyme = true;
	nomDeScene = p.getNom();
	prenom = p.getPrenom();
	nom = p_nomDeScene;
	genrePersonne = p.getGenre();
	niveau =0;

}


bool Super::estAnonyme()const
{
	return anonyme;
}

void Super::enregistrer()
{
	std::string temp = nomDeScene;
	nomDeScene = nom;
	nom = temp;
	anonyme = false;
}

Super & Super::getIdentite()
{
	if(anonyme==false)
	{
		return *this;
	}
	else
	{
		const char * terma="identite anonyme";
		throw AnonymeException(terma);
	}
}

void Super::setIdentite(Personne p)
{
	anonyme=true;
	nomDeScene = p.getNom();
	prenom = p.getPrenom();
	nom = p.getNom();
	genrePersonne = p.getGenre();
}

Super::Super(const Super & autre)
{
	nomDeScene = autre.nomDeScene;
	anonyme = autre.anonyme;
	genrePersonne = autre.genrePersonne;
	nom = autre.nom;
	prenom = autre.prenom;
	niveau = autre.niveau;

}

Super & Super::operator=(const Super & autre)
{
	if(this != & autre)
	{
		nomDeScene = autre.nomDeScene;
		anonyme = autre.anonyme;
		genrePersonne = autre.genrePersonne;
		nom = autre.nom;
		prenom = autre.prenom;
		niveau = autre.niveau;
	}
	return *this;
}

int Super::getNiveau()const
{
	return niveau;
}

void Super::ajouter(Capacite * ptr)
{
	int capacite = ptr->getCapacite();	
	//std::string nomCapacite = ptr->getNom(); // inutile pour le moment 
	niveau = niveau + capacite;
	
}

/**************MATERIEL*******/

Materiel::Materiel()
{
	nom = "";
	capacite = -1;
}

Materiel::~Materiel()
{

}

Materiel::Materiel(std::string p_nom, int p_capacite)
{
	nom = p_nom;
	capacite = p_capacite;
}

std::stringstream & Materiel::actionner(std::stringstream & ss)const
{
	ss<<nom<<" ["<<capacite<<"] en action";
	return ss;
}

std::stringstream & Materiel::exercer(std::stringstream & ss)const
{
	return ss;
}

std::stringstream & Materiel::penser(std::stringstream & ss)const
{
	return ss;
}
std::stringstream & Materiel::utiliser(std::stringstream & ss)const
{
	this->actionner(ss);
	return ss;
}

int Materiel::getCapacite()const
{
	return capacite;
}		

std::string Materiel::getNom()const
{
	return nom;
}

Materiel * Materiel::clone()const
{
	Materiel * ptr = new Materiel(this->getNom(),this->getCapacite());
	return ptr;
}
/*********************Physique***************************/


Physique::Physique()
{
	nom = "";
	capacite = -1;
}

Physique::~Physique()
{

}

Physique::Physique(std::string p_nom, int p_capacite)
{
	nom = p_nom;
	capacite = p_capacite;
	
}

std::stringstream & Physique::exercer(std::stringstream & ss)const
{
	ss<<nom<<" ["<<capacite<<"]";
	return ss;
}

std::stringstream & Physique::actionner(std::stringstream & ss)const
{
	return ss;
}

std::stringstream & Physique::penser(std::stringstream & ss)const
{
	return ss;
}
std::stringstream & Physique::utiliser(std::stringstream & ss)const
{
	this->exercer(ss);
	return ss;
}

int Physique::getCapacite()const
{
	return capacite;
}

std::string Physique::getNom()const
{
	return nom;
}

Materiel * Physique::clone()const
{
	Materiel * ptr = new Materiel();
	return ptr;
}


/***************************Psychique*************************/
Psychique::Psychique()
{
	nom = "";
	capacite = -1;
}

Psychique::~Psychique()
{

}

Psychique::Psychique(std::string p_nom, int p_capacite)
{
	nom= p_nom;
	capacite = p_capacite;
}


std::stringstream & Psychique::penser(std::stringstream & ss)const
{
	ss<<nom<<" ["<<capacite<<"]";
	return ss;
}
std::stringstream & Psychique::actionner(std::stringstream & ss)const
{
	return ss;
}

std::stringstream & Psychique::exercer(std::stringstream & ss)const
{
	return ss;
}

std::stringstream & Psychique::utiliser(std::stringstream & ss)const
{
	this->penser(ss);
	return ss;
}

int Psychique::getCapacite()const
{
	return capacite;
}

std::string Psychique::getNom()const
{
	return nom;
}

Materiel * Psychique::clone()const
{
	Materiel * ptr =new Materiel();
	return ptr;
}


/**********************Equipe********************/

Equipe::Equipe()
{
	niveau =0;
	nom = "";
}

Equipe::~Equipe()
{
	for (std::vector<Super *>::iterator it = monVec.begin(); it != monVec.end(); ++it)
	{
    	delete *it;
	}
}

void Equipe::ajouter(Super * ptr)
{
	int niveauCour = ptr->getNiveau();
	niveau = niveau + niveauCour;
	monVec.push_back(ptr);
}

int Equipe::getNombre()const
{
	int taille = monVec.size();
	return taille;
}

int Equipe::getNiveau()const
{
	return niveau;
}

Equipe::Equipe(std::string p_nom)
{
	niveau =0;
	nom = p_nom;
}

