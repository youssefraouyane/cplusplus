#ifndef marvel__hpp
#define marvel__hpp
#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include <exception>
#include <vector>
#include <iterator>


class Capacite;
class AnonymeException;
class Materiel;
class Personne
{
	public:
		enum Genre{HOMME, FEMME, INDETERMINE};
		Personne();
		Personne(const char * p_prenom, const char * p_nom,  Personne::Genre p_genrePersonne);
		~Personne();
		std::string getPrenom()const;
		std::string getNom()const;
		Genre getGenre()const;
		void afficher(std::stringstream & ss)const; 
		static Personne INCONNU;
		void setNom(std::string p_nom);
		void setPrenom(std::string p_prenom);
		void setGenre(Personne::Genre p_genrePersonne);
		Personne(const char * p_prenom, const char * p_nom);
		
	protected:
		std::string prenom;
		std::string nom;
		Genre genrePersonne;
		
};

std::stringstream& operator<<(std::stringstream & ss, const Personne & p);


bool operator==(const Personne & a, const Personne & b);
bool operator!=(const Personne & a, const Personne & b);


class Super:public Personne
{
	private:
		std::string nomDeScene;
		bool anonyme;
		int niveau;
	public:
		Super();
		~Super();
		Super(const Super & autre);
		Super & operator=(const Super & autre);
		Super(std::string p_nomDeScene, Personne p);
		bool estAnonyme()const;
		void enregistrer();
		Super & getIdentite();
		void setIdentite(Personne p);
		int getNiveau()const;
		void ajouter(Capacite * ptr);
};


class AnonymeException: public std::exception
{
	public:
    	AnonymeException(const char * terma="identite anonyme") throw()
    	{
			m_phrase = terma;
		}
 
     	virtual const char* what() const throw()
     	{
			return m_phrase;
     	}
    
    	virtual ~AnonymeException() throw()
    	{}
 
	private:

    	const char* m_phrase; //Description de l'erreur

};


class Capacite
{
	public:
		virtual std::stringstream & actionner(std::stringstream & ss)const=0;
		virtual std::stringstream & exercer(std::stringstream & ss)const=0;
		virtual std::stringstream & penser(std::stringstream & ss)const=0;
		virtual std::stringstream & utiliser(std::stringstream & ss)const=0;
		virtual int getCapacite()const=0;
		virtual std::string getNom()const=0;
		virtual Materiel * clone()const=0;
	protected:
		std::string nom;
		int capacite;
};

class Materiel:public Capacite
{		
	public:
		Materiel();
		virtual ~Materiel();
		Materiel(std::string p_nom, int p_capacite);
		virtual std::stringstream & actionner(std::stringstream & ss)const;
		virtual std::stringstream & exercer(std::stringstream & ss)const;
		virtual std::stringstream & penser(std::stringstream & ss)const;
		virtual std::stringstream & utiliser(std::stringstream & ss)const;
		virtual int getCapacite()const;
		virtual std::string getNom()const;
		virtual Materiel * clone()const;
		
};

class Physique:public Capacite
{
	public:
		Physique();
		~Physique();
		Physique(std::string p_nom, int p_capacite);
		virtual std::stringstream & exercer(std::stringstream & ss)const;
		virtual std::stringstream & actionner(std::stringstream & ss)const;
		virtual std::stringstream & penser(std::stringstream & ss)const;
		virtual std::stringstream & utiliser(std::stringstream & ss)const;
		virtual int getCapacite()const;
		virtual std::string getNom()const;
		virtual Materiel * clone()const;
};


class Psychique:public Capacite
{
	public:
		Psychique();
		virtual ~Psychique();
		Psychique(std::string p_nom, int p_capacite);
		virtual std::stringstream & penser(std::stringstream & ss)const;
		virtual std::stringstream & actionner(std::stringstream & ss)const;
		virtual std::stringstream & exercer(std::stringstream & ss)const;
		virtual std::stringstream & utiliser(std::stringstream & ss)const;
		virtual int getCapacite()const;
		virtual std::string getNom()const;
		virtual Materiel * clone()const;
};


class Equipe
{
	private:
		std::vector<Super*> monVec;
		int niveau;
		std::string nom;
	public:
		Equipe();
		~Equipe();
		Equipe(std::string p_nom);
		void ajouter(Super * ptr);
		int getNombre()const;
		int getNiveau()const;
};

#endif
