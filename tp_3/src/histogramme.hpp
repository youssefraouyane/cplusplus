#ifndef HISTOGRAMME_HPP_INCLUDED_
#define HISTOGRAMME_HPP_INCLUDED_

#include <iostream>
#include "valeur.hpp"
#include "echantillon.hpp"
#include "classe.hpp"

class Histogramme
{
	private:
		std::vector<Classe>vect;
		
	public:
		Histogramme(double pa, double pb, int quantite);
		const std::vector<Classe> & getClasses()const;
		~Histogramme();
		typedef std::vector<Classe> classes_t;
		typedef classes_t::const_iterator const_iterator;
				
	
};

Histogramme::Histogramme(double pa, double pb, int quantite)
{
	double pas = (pb - pa)/(quantite*1.0);
	double borneInf = pa;
	double borneSup = pa + pas;;
	for (int i=0;i<quantite;i++)
	{
		Classe c(borneInf, borneSup);
		vect.push_back(c);
		borneInf = borneInf + pas,
		borneSup = borneSup + pas;
	}
}

Histogramme::~Histogramme()
{

}

const std::vector<Classe> & Histogramme::getClasses()const
{
	return vect;
}






#endif
