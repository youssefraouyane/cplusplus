#ifndef CLASSE_HPP_INCLUDED_
#define CLASSE_HPP_INCLUDED_
#include <iostream>

class Classe
{
	private:
		double a;
		double b;
		int quantite;
	public:
		Classe();
		Classe(double pa, double pb);
		~Classe();
		Classe & operator=(const Classe & autre);
		Classe(const Classe & autre);
		double getBorneInf()const;
		double getBorneSup()const;
		double getQuantite()const;
		void setBorneInf(double pa);
		void setBorneSup(double pb);
		void setQuantite(int pQte);
		void ajouter();

};


Classe::Classe()
{
	a = 0.0;
	b = 0.0;
	quantite = 0;
}

Classe::~Classe()
{

}

Classe::Classe(double pa, double pb)
{
	a = pa;
	b = pb;
	quantite = 0;
}

Classe & Classe::operator=(const Classe & autre)
{
	if(this != & autre)
	{
		a = autre.getBorneInf();
		b = autre.getBorneSup();
		quantite = autre.getQuantite();
	}
	return *this;
}


Classe::Classe(const Classe & autre)
{
	a = autre.getBorneInf();
	b = autre.getBorneSup();
	quantite = autre.getQuantite();
}

double Classe::getBorneInf()const
{
	return a;
}

double Classe::getBorneSup()const
{
	return b;
}

double Classe::getQuantite()const
{
	return quantite;
}

void Classe::setBorneInf(double pa)
{
	a = pa;
}

void Classe::setBorneSup(double pb)
{
	b = pb;
}

void Classe::setQuantite(int pQte)
{
	quantite = pQte;
}

void Classe::ajouter()
{
	quantite++;
}

#endif 
