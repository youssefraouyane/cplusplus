#ifndef ECHANTILLON_HPP_INCLUDED_
#define ECHANTILLON_HPP_INCLUDED_
#include <iostream>
#include <vector>

class Echantillon
{

	private:
		int taille;
		std::vector<Valeur> monVect;
	public:
		Echantillon();
		~Echantillon();
		void ajouter(double v);
		int getTaille()const;
		Valeur getMinimum()const;
		Valeur getMaximum()const;
		Valeur getValeur(int index)const;
};

Echantillon::Echantillon()
{
	taille = 0;
}

Echantillon::~Echantillon()
{

}

Valeur Echantillon::getValeur(int index)const
{
	Valeur v(0.0);
	if(index <taille)
	{
		v = monVect[index];
	}
	else
	{
		throw std::out_of_range("Indice hors de portee");
	}
	return v;
		
}

void Echantillon::ajouter(double v)
{
	Valeur val(v);
	monVect.push_back(v);
	taille++;
}

int Echantillon::getTaille()const
{
	return taille;
}

Valeur Echantillon::getMinimum()const
{
	Valeur v;
	if(taille > 0)
	{
		v = monVect[0];
		for(int i=1;i<taille;i++)
		{
			if(monVect[i] < v)
			{
				v = monVect[i];
			}
		}
	}
	else
	{
		throw std::domain_error("erreur d'indice");
	}
	return v;
	
}

Valeur Echantillon::getMaximum()const
{
	Valeur v;
	if(taille > 0)
	{
		v = monVect[0];
		for(int i=1;i<taille;i++)
		{
			if(! (monVect[i] < v))
			{
				v = monVect[i];
			}
		}
	}
	else
	{
		throw std::domain_error("erreur d'indice");
	}
	return v;

}

#endif
