#ifndef VALEUR_HPP_INCLUDED_
#define VALEUR_HPP_INCLUDED_
#include <iostream>


class Valeur
{
	private:
		double nombre;
	public:
		Valeur(double p_nombre=0.0);
		~Valeur();
		Valeur(const Valeur & autre);
		Valeur & operator=(const Valeur & autre);
		double getNombre()const;
		void setNombre(double p_nombre);
		bool operator<(const Valeur & autre)const;
		
};

Valeur::Valeur(double p_nombre)
{
	nombre = p_nombre;
}
Valeur::~Valeur()
{

}
Valeur::Valeur(const Valeur & autre)
{
	nombre = autre.getNombre();
}

Valeur & Valeur::operator=(const Valeur & autre)
{
	if(this != & autre)
	{
		nombre = autre.getNombre();
	}
	return *this;
}

double Valeur::getNombre()const
{
	return nombre;
}

void Valeur::setNombre(double p_nombre)
{
	nombre = p_nombre;
}

bool Valeur::operator<(const Valeur & autre)const
{
	bool comparaison = false;
	if(this->getNombre()<autre.getNombre())
	{
		comparaison = true;
	}
	return comparaison;
}

#endif
