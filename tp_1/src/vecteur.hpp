#ifndef Vecteur_HPP_INCLUDED_
#define Vecteur_HPP_INCLUDED_
#include <iostream>

class Vecteur
{
	private: 
		int *array;
		int taille;
		
	public: 
		Vecteur();
		Vecteur(int pTaille);
		~Vecteur();
		Vecteur(const Vecteur & autre);
		int getTaille()const;
		int* getArray()const;
		Vecteur & operator=(const Vecteur & autre);
		friend std::ostream & operator<<(std::ostream & out, const Vecteur & obj); 
		void setTaille(int pTaille);
		void setArray(int * pArray);
		void ecriture(int px, int indice); // private ??
		friend Vecteur operator+(Vecteur & a, Vecteur & b);
};
std::ostream & operator<<(std::ostream & out, const Vecteur & obj); 
Vecteur operator+(Vecteur & a, Vecteur & b);

#endif
