#ifndef Point_HPP_INCLUDED_
#define Point_HPP_INCLUDED_

#include <iostream>
#include <sstream>
#include <cmath>
class Polaire;
class Cartesien;
class Point
{
	public:
		virtual void afficher(std::stringstream & flux)const=0;
		virtual void convertir(Polaire & p)const=0;
		virtual void convertir(Cartesien & c)const=0;		
		virtual ~Point(){};
		friend std::stringstream & operator<<(std::stringstream & out, const Point & p);
};
std::stringstream & operator<<(std::stringstream & out, const Point & p);

#endif
