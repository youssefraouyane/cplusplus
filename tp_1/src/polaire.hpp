#ifndef Polaire_HPP_INCLUDED_
#define Polaire_HPP_INCLUDED_

#include <iostream>
#include <sstream>

#include "point.hpp"
#include "cartesien.hpp"

class Cartesien;
class Polaire : public Point
{
	private: 
		double angle;
		double distance;
	public:
		Polaire();
		Polaire(const Cartesien & c);
		virtual ~Polaire();
		Polaire(const Polaire & p);
		Polaire & operator=(const Polaire & autre); 
		Polaire(double pAngle, double pDistance);
		double getAngle()const;
		double getDistance()const;
		void setAngle(double pAngle);
		void setDistance(double pDistance);
		virtual void afficher(std::stringstream & flux)const;
		friend std::stringstream & operator<<(std::stringstream & out, const Polaire & p);
		virtual void convertir(Cartesien & c)const;
		virtual void convertir(Polaire & p)const;
};

std::stringstream & operator<<(std::stringstream & out, const Polaire & p);

#endif
