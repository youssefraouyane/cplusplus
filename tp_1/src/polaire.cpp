#include "polaire.hpp"

Polaire::Polaire()
{
	angle=0.0;
	distance=0.0;
}

Polaire::~Polaire()
{
}
Polaire::Polaire(double pAngle, double pDistance)
{
	angle = pAngle;
	distance = pDistance;
}

double Polaire::getAngle()const
{
	return angle;
}

double Polaire::getDistance()const
{
	return distance;
}

void Polaire::setAngle(double pAngle)
{
	angle = pAngle;
}
		
void Polaire::setDistance(double pDistance)
{
	distance = pDistance;
}

void Polaire::afficher(std::stringstream & flux)const
{
	flux<<"(a="<<this->getAngle()<<";d="<<this->getDistance()<<")";
}

std::stringstream & operator<<(std::stringstream & out, const Polaire & p)
{
	p.afficher(out);
	return out;
}

void Polaire::convertir(Cartesien & c)const
{
	c.setX(this->getDistance()*cos(this->getAngle()*M_PI/180));
	c.setY(this->getDistance()*sin(this->getAngle()*M_PI/180));
}


void Polaire::convertir(Polaire & p)const
{
	p.setAngle(this->getAngle());
	p.setDistance(this->getDistance());
}

Polaire::Polaire(const Polaire & p)
{
	angle = p.getAngle();
	distance = p.getDistance();
}
Polaire & Polaire::operator=(const Polaire & autre)
{
	if(this != & autre)
	{
		angle = autre.getAngle();
		distance = autre.getDistance();
	}
	return *this;
}

Polaire::Polaire(const Cartesien & c)
{
	Polaire();
	c.convertir(*this);
}

