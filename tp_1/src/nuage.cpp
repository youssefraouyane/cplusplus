#include "nuage.hpp"

Nuage::Nuage()
{
	zineOLaTaille = 0;
} 

Nuage::~Nuage()
{

}

int Nuage::size()const
{
	return zineOLaTaille;
}

Nuage::Nuage(const Nuage & autre)
{
	zineOLaTaille = autre.size();
}


Nuage & Nuage::operator=(const Nuage & autre)
{
	if(this != & autre)
	{
		zineOLaTaille = autre.size();
	}
	return *this;
}

void Nuage::ajouter(Point & elt)
{
	zineOLaTaille++;
	vect.push_back(& elt);
}
const std::vector <Point*> Nuage::getVecteur()const
{
	return vect;
}
/*std::vector<Point*>::const_iterator Nuage::begin()
{
	return vect.begin();
}*/

/*Nuage::const_iterator Nuage::begin()const 
{
	const_iterator it_;
	const std::vector <Point*> vect_ = this->getVecteur();
	it_.setIterator(vect_.begin());
	return it_;
	
}*/

Cartesien barycentre(Nuage & mNuage)
{
	const std::vector <Point*> myVect = mNuage.getVecteur();
	std::vector <Point*>::const_iterator it;
	int taille = mNuage.size();
	double mTaille = 1.0*taille;
	double xBarycentre = 0.0;
	double yBarycentre = 0.0;
	for (it = myVect.begin(); it != myVect.end();it++)
	{
		Cartesien * m_cartesien = dynamic_cast<Cartesien*>(*it);
		xBarycentre = xBarycentre + m_cartesien->getX();
		yBarycentre = yBarycentre + m_cartesien->getY();
	}
	Cartesien b(xBarycentre/mTaille, yBarycentre/mTaille);
	return b;
}





