#include <iostream>
//#include "point.hpp"
#include "cartesien.hpp"
#ifndef NUAGE_HPP_INCLUDED_
#define NUAGE_HPP_INCLUDED_

#include "polaire.hpp"
#include <vector>
#include <iterator>
class Nuage
{
	private : 
		int zineOLaTaille;
		std::vector <Point*> vect;
		
	public:
		Nuage();
		~Nuage();
		int size()const;
		Nuage(const Nuage & autre);
		Nuage & operator=(const Nuage & autre);
		const std::vector <Point*> getVecteur()const;
		void ajouter(Point & elt);
		typedef std::vector<Point *>::const_iterator const_iterator;
    const_iterator begin() const {
        return vect.begin();
    }

    const_iterator end() const {
        return vect.end();
    }
		

};
Cartesien barycentre(Nuage & mNuage);

class BarycentreCartesien
{
	public: 
		Cartesien operator()(Nuage & mNuage)
		{
			Cartesien b = barycentre(mNuage);
			return b;
		}

};

class BarycentrePolaire
{
	public:
		Polaire operator()(Nuage & mNuage)
		{
			Cartesien c = barycentre(mNuage);
			Polaire p(c);
			return p;
		}
		

};
#endif





