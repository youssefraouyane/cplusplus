#include "vecteur.hpp"

Vecteur::Vecteur()
{
	taille = 0;
	array = nullptr; 
}

Vecteur::Vecteur(int pTaille)
{
	taille = pTaille;
	array = new int[taille];
}

Vecteur::~Vecteur()
{
	delete [] array;
}

Vecteur::Vecteur(const Vecteur & autre)
{
	taille = autre.getTaille();
	delete [] array;
	array = new int[taille];
	for (int i = 0; i<taille; i++)
	{
		array[i] = autre.array[i];
	}

}

Vecteur & Vecteur::operator=(const Vecteur & autre)
{
	if(this != &autre)
	{
		taille = autre.getTaille();
		delete [] array;
		int * arrayCopy = autre.getArray();
		array = new int[taille];
		for (int i=0;i<taille;i++)
		{
			array[i]= arrayCopy[i];
		}
	}
	return *this;
}

int Vecteur::getTaille()const
{
	return taille;
}

int* Vecteur::getArray()const
{
	return array;
}

std::ostream & operator<<(std::ostream & out, const Vecteur & obj)
{
	out<<"zeb zeb zeb zeb"<<std::endl;
	return out;
}

/*void Vecteur::ecriture(int px, int indice)
{
	if(indice <= this->getTaille())
	{
		array[indice] = px;
	}
} 

void Vecteur::setTaille(int pTaille)
{
	taille = pTaille;
	delete [] array;
	array = nullptr;	
}

void Vecteur::setArray(int * pArray)
{
	//size_t data = ;
	//taille = static_cast<int>(data);
	int cpt = 0;
	delete [] array;
	array = new int[taille];
	for (int i=0; i < sizeof(pArray)/sizeof(pArray[0]); i++)
	{
		array[i] = pArray[i];
		cpt++;
	}
	taille = cpt;
	
}

Vecteur operator+(Vecteur & a, Vecteur & b)
{
	Vecteur retour;
	int * aVecteur = a.getArray();
	int * bVecteur = b.getArray();
	int cpt = 0;
	int * pArray = new int[a.getTaille() + b.getTaille()];
	for (int i=0; i < sizeof(aVecteur)/sizeof(*aVecteur); i++)
	{
		pArray[i] = aVecteur[i];
		cpt++;
	}
	for (int i=cpt; i < sizeof(aVecteur)/sizeof(*aVecteur) + sizeof(bVecteur)/sizeof(*bVecteur); i++)
	{
		pArray[i] = bVecteur[i];
		cpt++;
	}
	retour.setArray(pArray);
	retour.setTaille(cpt);
	return retour;
}*/

