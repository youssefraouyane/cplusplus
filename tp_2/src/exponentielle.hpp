#ifndef Exponentielle_HPP_INCLUDED_
#define Exponentielle_HPP_INCLUDED_
#include "factorielle.hpp"
#include "puissance.hpp"


template<int N>
class Exponentielle
{
	public:
		static unsigned long valeur(double x)
		{
			unsigned long xn = Puissance<N>::valeur(x)/Factorielle<N>::valeur;
			return   xn + Exponentielle<N-1>::valeur(x);
		}


};

template<>
class Exponentielle<0>
{
	public: 
		static unsigned long valeur(double x)
		{
			unsigned long retour = 1.00000000000000;
			return retour;
		}


}; 



#endif
