#ifndef Puissance_HPP_INCLUDED_
#define Puissance_HPP_INCLUDED_

template<int M>
class Puissance
{
	public:
		static unsigned long valeur(double x)
		{
			return x*Puissance<M-1>::valeur(x);
		}


};

template<>
class Puissance<0>
{
	public: 
		static unsigned long valeur(double x)
		{
			unsigned long retour = 1.0;
			return retour;
		}


}; 

#endif
