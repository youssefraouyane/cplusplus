#ifndef Cosinus_INCLUDED_HPP_
#define Cosinus_INCLUDED_HPP_
#include "factorielle.hpp"
#include "puissance.hpp"

template<int N>
class Cosinus
{
	public:
		static unsigned long valeur(double x)
		{
			const int M = 2*N;
			unsigned long cos_xn = Puissance<M>::valeur(x)*Puissance<N>::valeur(-1.0)/Factorielle<M>::valeur;
			const int P = 2*N -2;
			unsigned long returnValue = cos_xn + Cosinus<P>::valeur(x);
			return returnValue;
		}

	

};

template<>
class Cosinus<0>
{
	public:
		static unsigned long valeur(double x)
		{
			unsigned long cos0 = 1.0;
			return cos0;
		}


};
/*
template<int N>
class Sinus
{
	public:
		static unsigned long valeur(double x)
		{
			const int M = 2*N+1;
			unsigned long cos_xn = Puissance<M>::valeur(x)*Puissance<N>::valeur(-1.0)/Factorielle<M>::valeur;
			unsigned long returnValue = cos_xn + Cosinus<N-1>::valeur(x);
			return returnValue;
			
		}

};
*/

#endif
