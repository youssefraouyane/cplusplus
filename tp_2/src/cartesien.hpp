#ifndef Cartesien_HPP_INCLUDED_
#define Cartesien_HPP_INCLUDED_

#include "point.hpp"
#include <iostream>
#include <sstream>
#include "polaire.hpp" 

class Polaire;
class Cartesien : public Point
{
	private:
		double x;
		double y;
	public:
		Cartesien();
		Cartesien(const Polaire &p);
		virtual ~Cartesien();
		Cartesien( const Cartesien & c);
		Cartesien & operator=(const Cartesien & autre); 
		Cartesien(double px, double py);
		double getX()const;
		double getY()const;
		void setX(double px);
		void setY(double py);
		virtual void afficher(std::stringstream& flux)const;
		friend std::stringstream & operator<<(std::stringstream & out, const Cartesien & c);
		virtual void convertir(Polaire & p)const;
		virtual void convertir(Cartesien & c)const;		
};

std::stringstream & operator<<(std::stringstream & out, const Cartesien & c);
#endif
