#ifndef NUAGE_HPP_INCLUDED_
#define NUAGE_HPP_INCLUDED_
#include "cartesien.hpp"
#include <iostream>
#include <vector>
#include <cmath>

template <typename T>
class Nuage
{

	private:
		std::vector<T> monVecteur;
	public:
		Nuage();
		~Nuage();
		void ajouter(T elt);
		int size()const
		{
			return monVecteur.size();
		}
		typedef typename std::vector<T>::const_iterator const_iterator;// LOIC
		const_iterator begin() const
		{
			return monVecteur.begin();
		}
		const_iterator end() const
		{
			return monVecteur.end();
		}
		const std::vector<T> & getMonVecteur() const 
		{ 
			return monVecteur; 
		}

};
template<typename T>Nuage<T>::Nuage()
{

}
template<typename T> Nuage<T>::~Nuage()
{


}

template<typename T> void Nuage<T>::ajouter(T elt)
{
	monVecteur.push_back(elt);

}
template <typename T> T barycentre_v1(Nuage<T> & n)
{
	T retour;
	return retour;

}

template <> 
Cartesien barycentre_v1<Cartesien>(Nuage<Cartesien> & n) 
{ 
	double x = 0.0;
	double y = 0.0;
	const std::vector<Cartesien> monVecteur_ = n.getMonVecteur();
	unsigned int taille = n.size();
	for(unsigned int i=0;i<taille;i++)
	{
		x = x + monVecteur_[i].getX();
		y = y + monVecteur_[i].getY();
	} 
	double tailleDouble = taille*1.0;
	if(tailleDouble>0)
	{
		x = x/tailleDouble;
		y = y/tailleDouble;
	}
    Cartesien retour(x,y);
	return retour;
} 

template <> 
Polaire barycentre_v1<Polaire>(Nuage<Polaire> & n) 
{ 
	double distance = 0.0;
	double angle = 0.0;
	const std::vector<Polaire> monVecteur_ = n.getMonVecteur();
	double x =0.0;
	double y = 0.0;
	int taille = n.size();
	for(int i=0;i<taille;i++)
	{
		x = x + monVecteur_[i].getDistance()*cos(monVecteur_[i].getAngle());
		y = y + monVecteur_[i].getDistance()*sin(monVecteur_[i].getAngle());
		distance = distance + monVecteur_[i].getDistance();
		angle = angle + monVecteur_[i].getAngle();
	} 
	double tailleDouble = taille*1.0;
	if(tailleDouble>0)
	{
		x = x/tailleDouble;
		y = y/tailleDouble;
		distance = distance/tailleDouble;
		angle = angle/tailleDouble;
	}
	double distanceTest4a = sqrt(pow(x,2)+pow(y,2));
	double angleTest4a = atan(y/x);
	Polaire retourTest4a(angleTest4a, distanceTest4a);
    Polaire retour(angle,distance);
	//return retourTest4a; //test4a
	return retour;
} 

template <typename T> T barycentre_v2(Nuage<T> & n)
{
	T retour;
	return retour;

}

template <typename T> T barycentre_v2(std::vector<T> & vect)
{
	T retour;
	return retour;

}
template<>
Cartesien barycentre_v2<Cartesien>(Nuage<Cartesien> & n) 
{ 
	double x = 0.0;
	double y = 0.0;
	const std::vector<Cartesien> monVecteur_ = n.getMonVecteur();
	unsigned int taille = n.size();
	for(unsigned int i=0;i<taille;i++)
	{
		x = x + monVecteur_[i].getX();
		y = y + monVecteur_[i].getY();
	} 
	double tailleDouble = taille*1.0;
	if(tailleDouble>0)
	{
		x = x/tailleDouble;
		y = y/tailleDouble;
	}
    Cartesien retour(x,y);
	return retour;
} 
template<>
Cartesien barycentre_v2<Cartesien>(std::vector<Cartesien> & vect)
{
	Cartesien c1(0.0,0.0);
	int taille = vect.size();
	for(int i=0;i<taille;i++)
	{
		c1.setX(c1.getX()+vect[i].getX());
		c1.setY(c1.getY()+vect[i].getY());
	}
	double tailleDouble = taille*1.0;
	if(tailleDouble >0.0)
	{
		c1.setX(c1.getX()/tailleDouble);
		c1.setY(c1.getY()/tailleDouble);
	}
		
	return c1;
	for(int i=0;i<taille;i++)
	{
		c1.setX(c1.getX()+vect[i].getX());
		c1.setY(c1.getY()+vect[i].getY());
	}
}

template<>
Polaire barycentre_v2<Polaire>(std::vector<Polaire> & vect)
{
	Polaire p1(0.0,0.0);
	int taille = vect.size();
	double x = 0.0;
	double y = 0.0;
	for(int i=0;i<taille;i++)
	{
		double r = vect[i].getDistance();
		double theta = vect[i].getAngle();
		x = x + r*cos(theta);
		y = y + r*sin(theta);	
	}	
	if(taille > 0)
	{
		double tailleDouble = taille*1.0;
		x = x/tailleDouble;
		y = y/tailleDouble;
		p1.setDistance(sqrt(pow(x,2)+pow(y,2)));
		p1.setAngle(atan(y/x));
	}
	
	return p1;

}

#endif
