#include "cartesien.hpp"



Cartesien::Cartesien()
{
	x=0.0;
	y=0.0;
}

Cartesien::~Cartesien()
{
}

Cartesien::Cartesien(double px, double py)
{
	x=px;
	y=py;
}

double Cartesien::getX()const
{
	return x;
}

double Cartesien::getY()const
{
	return y;
}

void Cartesien::setX(double px)
{
	x=px;
}

void Cartesien::setY(double py)
{
	y=py;
}
void Cartesien::afficher(std::stringstream & flux)const
{
	flux<<"(x="<<this->getX()<<";y="<<this->getY()<<")";
}


std::stringstream & operator<<(std::stringstream & out, const Cartesien & c)
{
	c.afficher(out);
	return out;

}

void Cartesien::convertir(Polaire & p)const
{
	p.setDistance(sqrt(pow(this->getX(),2)+pow(this->getY(),2)));
	double cartesienY = this->getY();
	double cartesienX = this->getX();
	p.setAngle(atan(cartesienY / cartesienX)*180/M_PI);
}

void Cartesien::convertir(Cartesien & c)const
{
	c.setX(this->getX());
	c.setY(this->getY());
}
Cartesien::Cartesien( const Cartesien & c)
{
	x = c.getX();
	y = c.getY();
}
Cartesien & Cartesien::operator=(const Cartesien & autre)
{
	if( this != & autre)
	{
		x = autre.getX();
		y = autre.getY();
	}
	return *this;
}

Cartesien::Cartesien(const Polaire &p)
{
	Cartesien();
	p.convertir(*this);
}
